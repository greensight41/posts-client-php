<?php

namespace Posts\PostsServiceClient;

class PostsServiceClientProvider
{
    /** @var string[] */
    public static $apis = ['\Posts\PostsServiceClient\Api\VotesApi', '\Posts\PostsServiceClient\Api\PostsApi'];

    /** @var string[] */
    public static $dtos = [
        '\Posts\PostsServiceClient\Dto\SearchPostsRequest',
        '\Posts\PostsServiceClient\Dto\SearchVotesFilter',
        '\Posts\PostsServiceClient\Dto\PaginationTypeOffsetEnum',
        '\Posts\PostsServiceClient\Dto\PostResponse',
        '\Posts\PostsServiceClient\Dto\PaginationTypeEnum',
        '\Posts\PostsServiceClient\Dto\VoteIncludes',
        '\Posts\PostsServiceClient\Dto\Vote',
        '\Posts\PostsServiceClient\Dto\VoteReadonlyProperties',
        '\Posts\PostsServiceClient\Dto\SearchPostsResponse',
        '\Posts\PostsServiceClient\Dto\VoteFillableProperties',
        '\Posts\PostsServiceClient\Dto\Post',
        '\Posts\PostsServiceClient\Dto\VoteForCreate',
        '\Posts\PostsServiceClient\Dto\ResponseBodyOffsetPagination',
        '\Posts\PostsServiceClient\Dto\PostVotesFillableProperties',
        '\Posts\PostsServiceClient\Dto\PostVotesReadonlyProperties',
        '\Posts\PostsServiceClient\Dto\RequestBodyOffsetPagination',
        '\Posts\PostsServiceClient\Dto\SearchPostsFilter',
        '\Posts\PostsServiceClient\Dto\VotePost',
        '\Posts\PostsServiceClient\Dto\VoteForReplace',
        '\Posts\PostsServiceClient\Dto\SearchPostsResponseMeta',
        '\Posts\PostsServiceClient\Dto\PostForCreate',
        '\Posts\PostsServiceClient\Dto\Error',
        '\Posts\PostsServiceClient\Dto\RequestBodyPagination',
        '\Posts\PostsServiceClient\Dto\ModelInterface',
        '\Posts\PostsServiceClient\Dto\ResponseBodyPagination',
        '\Posts\PostsServiceClient\Dto\PostVotes',
        '\Posts\PostsServiceClient\Dto\PaginationTypeCursorEnum',
        '\Posts\PostsServiceClient\Dto\ResponseBodyCursorPagination',
        '\Posts\PostsServiceClient\Dto\SearchVotesRequest',
        '\Posts\PostsServiceClient\Dto\RequestBodyCursorPagination',
        '\Posts\PostsServiceClient\Dto\VoteResponse',
        '\Posts\PostsServiceClient\Dto\PostForReplace',
        '\Posts\PostsServiceClient\Dto\ErrorResponse',
        '\Posts\PostsServiceClient\Dto\VotePostReadonlyProperties',
        '\Posts\PostsServiceClient\Dto\PostIncludes',
        '\Posts\PostsServiceClient\Dto\VotePostFillableProperties',
        '\Posts\PostsServiceClient\Dto\SearchVotesResponse',
        '\Posts\PostsServiceClient\Dto\PostFillableProperties',
        '\Posts\PostsServiceClient\Dto\EmptyDataResponse',
        '\Posts\PostsServiceClient\Dto\PostReadonlyProperties',
    ];

    /** @var string */
    public static $configuration = '\Posts\PostsServiceClient\Configuration';
}
