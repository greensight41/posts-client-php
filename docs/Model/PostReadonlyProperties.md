# # PostReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | [optional] 
**votes_count** | **int** | Суммарный рейтинг из Votes | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания поста | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


