# # VoteIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**votes** | [**\Posts\PostsServiceClient\Dto\VotePost[]**](VotePost.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


