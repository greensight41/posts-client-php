# # PostForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **string** | Заголовок поста | [optional] 
**body** | **string** | текст поста | [optional] 
**user_id** | **int** | идентификатор пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


