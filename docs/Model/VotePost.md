# # VotePost

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поста | [optional] 
**title** | **int** | Заголовок поста | [optional] 
**body** | **int** | Текст поста | [optional] 
**votes_count** | **int** | Общее количество голосов | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


