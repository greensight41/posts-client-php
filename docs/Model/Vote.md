# # Vote

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор голоса | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания голоса | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления голоса | [optional] 
**post_id** | **int** | Идентификатор голоса | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 
**vote_type** | **bool** | Тип голоса | [optional] 
**votes** | [**\Posts\PostsServiceClient\Dto\VotePost[]**](VotePost.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


