# # SearchVotesResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Posts\PostsServiceClient\Dto\Vote[]**](Vote.md) |  | 
**meta** | [**\Posts\PostsServiceClient\Dto\SearchPostsResponseMeta**](SearchPostsResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


