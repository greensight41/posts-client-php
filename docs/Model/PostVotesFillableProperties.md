# # PostVotesFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**post_id** | **int** | Идентификатор поста | [optional] 
**user_id** | **int** | Идентификатор юзера | [optional] 
**vote_type** | **bool** | Тип голоса | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


