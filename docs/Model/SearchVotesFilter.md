# # SearchVotesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор голоса | [optional] 
**post_id** | **int** | Идентификатор голоса | [optional] 
**user_id** | **int** | Идентификатор пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


