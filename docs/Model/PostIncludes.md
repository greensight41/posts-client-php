# # PostIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**votes** | [**\Posts\PostsServiceClient\Dto\PostVotes[]**](PostVotes.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


