# Posts\PostsServiceClient\PostsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPost**](PostsApi.md#createPost) | **POST** /posts/posts | Создание объекта типа Post
[**deletePost**](PostsApi.md#deletePost) | **DELETE** /posts/posts/{id} | Удаление объекта типа Post
[**getPosts**](PostsApi.md#getPosts) | **GET** /posts/posts/{id} | Получение объекта типа Post
[**replacePost**](PostsApi.md#replacePost) | **PATCH** /posts/posts/{id} | Обновление объекта типа Post
[**searchPosts**](PostsApi.md#searchPosts) | **POST** /posts/posts:search | Поиск объектов типа Posts



## createPost

> \Posts\PostsServiceClient\Dto\PostResponse createPost($post_for_create)

Создание объекта типа Post

Создание объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$post_for_create = new \Posts\PostsServiceClient\Dto\PostForCreate(); // \Posts\PostsServiceClient\Dto\PostForCreate | 

try {
    $result = $apiInstance->createPost($post_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->createPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **post_for_create** | [**\Posts\PostsServiceClient\Dto\PostForCreate**](../Model/PostForCreate.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deletePost

> \Posts\PostsServiceClient\Dto\EmptyDataResponse deletePost($id)

Удаление объекта типа Post

Удаление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deletePost($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->deletePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Posts\PostsServiceClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPosts

> getPosts($id, $include)

Получение объекта типа Post

Получение объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $apiInstance->getPosts($id, $include);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->getPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replacePost

> \Posts\PostsServiceClient\Dto\PostResponse replacePost($id, $post_for_replace)

Обновление объекта типа Post

Обновление объекта типа Post

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$post_for_replace = new \Posts\PostsServiceClient\Dto\PostForReplace(); // \Posts\PostsServiceClient\Dto\PostForReplace | 

try {
    $result = $apiInstance->replacePost($id, $post_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->replacePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **post_for_replace** | [**\Posts\PostsServiceClient\Dto\PostForReplace**](../Model/PostForReplace.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\PostResponse**](../Model/PostResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchPosts

> \Posts\PostsServiceClient\Dto\SearchPostsResponse searchPosts($search_posts_request)

Поиск объектов типа Posts

Поиск объектов типа Posts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\PostsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_posts_request = new \Posts\PostsServiceClient\Dto\SearchPostsRequest(); // \Posts\PostsServiceClient\Dto\SearchPostsRequest | 

try {
    $result = $apiInstance->searchPosts($search_posts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PostsApi->searchPosts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_posts_request** | [**\Posts\PostsServiceClient\Dto\SearchPostsRequest**](../Model/SearchPostsRequest.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\SearchPostsResponse**](../Model/SearchPostsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

