# Posts\PostsServiceClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](VotesApi.md#createVote) | **POST** /votes/votes | Создание объекта типа Vote
[**deleteVote**](VotesApi.md#deleteVote) | **DELETE** /votes/votes/{id} | Удаление объекта типа Vote
[**replaceVote**](VotesApi.md#replaceVote) | **PATCH** /votes/votes/{id} | Обновление объекта типа Vote
[**searchVotes**](VotesApi.md#searchVotes) | **POST** /votes/votes:search | Поиск объектов типа Votes



## createVote

> \Posts\PostsServiceClient\Dto\VoteResponse createVote($vote_for_create)

Создание объекта типа Vote

Создание объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$vote_for_create = new \Posts\PostsServiceClient\Dto\VoteForCreate(); // \Posts\PostsServiceClient\Dto\VoteForCreate | 

try {
    $result = $apiInstance->createVote($vote_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vote_for_create** | [**\Posts\PostsServiceClient\Dto\VoteForCreate**](../Model/VoteForCreate.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \Posts\PostsServiceClient\Dto\EmptyDataResponse deleteVote($id)

Удаление объекта типа Vote

Удаление объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Posts\PostsServiceClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceVote

> \Posts\PostsServiceClient\Dto\VoteResponse replaceVote($id, $vote_for_replace)

Обновление объекта типа Vote

Обновление объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$vote_for_replace = new \Posts\PostsServiceClient\Dto\VoteForReplace(); // \Posts\PostsServiceClient\Dto\VoteForReplace | 

try {
    $result = $apiInstance->replaceVote($id, $vote_for_replace);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->replaceVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **vote_for_replace** | [**\Posts\PostsServiceClient\Dto\VoteForReplace**](../Model/VoteForReplace.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVotes

> \Posts\PostsServiceClient\Dto\SearchVotesResponse searchVotes($search_votes_request)

Поиск объектов типа Votes

Поиск объектов типа Votes

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Posts\PostsServiceClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_votes_request = new \Posts\PostsServiceClient\Dto\SearchVotesRequest(); // \Posts\PostsServiceClient\Dto\SearchVotesRequest | 

try {
    $result = $apiInstance->searchVotes($search_votes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchVotes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_votes_request** | [**\Posts\PostsServiceClient\Dto\SearchVotesRequest**](../Model/SearchVotesRequest.md)|  |

### Return type

[**\Posts\PostsServiceClient\Dto\SearchVotesResponse**](../Model/SearchVotesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

